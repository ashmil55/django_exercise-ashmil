from django.urls import path
from . import views
from .views import read_write

urlpatterns = [

    path('', views.read_write, name = 'monitor-home'),

    ]
