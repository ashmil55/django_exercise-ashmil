from django.shortcuts import render
from .models import MonitoredInterface
import requests
from django.http import HttpRequest, HttpResponse

# Create your views here.
BASE_URL_ALPHA='https://next.json-generator.com/api/json/get/41wV8bj_O'
BASE_URL_BRAVO='https://next.json-generator.com/api/json/get/Nk48cbjdO'

def read_write(request):

    data1 = requests.get(BASE_URL_ALPHA).json()
    data2 = requests.get(BASE_URL_BRAVO).json()

    existing_data = MonitoredInterface.objects.all()

    for i in data1 :
        external_id = i['_id']
        mac_address = i['mac'].replace(":", "") #the first json data has every mac address in XX:XX:XX:XX:XX:XX format
        ipv4_address = i['address']
        interface_id = i['guid']
        interface_name = i['interface']

        load_query = MonitoredInterface.objects.update_or_create( mac_address = mac_address, interface_name = interface_name,
        defaults  = {'external_id' : external_id, 'ipv4_address' : ipv4_address, 'interface_id' : interface_id, 'mac_address' : mac_address, 'interface_name' : interface_name} )

        print(load_query)

        load_query[0].save()


    for i in data2 :
        external_id = i['id']
        mac_address = i['mac_address']
        for j in i['interfaces']:
            ipv4_address = j['ipv4']
            interface_id = j['id']
            interface_name = j['name']
            load_query = MonitoredInterface.objects.update_or_create( mac_address = mac_address, interface_name = interface_name,
            defaults  = {'external_id' : external_id, 'ipv4_address' : ipv4_address, 'interface_id' : interface_id, 'mac_address' : mac_address, 'interface_name' : interface_name} )

            load_query[0].save()

        #ipv4_address = i['interfaces']['ipv4']
        #interface_id = i['interfaces']['id']
        #interface_name = i['interfaces']['name']


    return HttpResponse("The date from the urls has been loaded to the database" )
