from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone

# Create your models here.
class MonitoredInterface(models.Model) :

    alphanumeric = RegexValidator(regex = r'^[0-9A-Z]{12}$', message = 'Mac Address should have 12 characters containing only alphanumeric values')

    #we have not set primary key constraint for external_id because the BASE_URL_BRAVO had data which had multiple records for same external_id
    external_id = models.TextField()

    #regex validator for mac address ... in the views.py file we will write a code to replace : with ""
    mac_address = models.CharField(max_length=12, validators=[alphanumeric])
    ipv4_address = models.GenericIPAddressField()
    interface_id = models.TextField()
    interface_name = models.TextField()

    #auto_now_add attribute adds datetime only when the record is created for the first time
    created_at = models.DateTimeField(auto_now_add=True)
    #add datatime to updated_at field everytime it is created
    updated_at = models.DateTimeField(auto_now=True)

    class Meta():
        #app_label = 'MonitoredInterface'
        unique_together = ('mac_address','interface_name')
